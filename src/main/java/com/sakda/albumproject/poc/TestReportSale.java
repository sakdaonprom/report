/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.albumproject.poc;

import com.sakda.albumproject.model.ReportSale;
import com.sakda.albumproject.service.ReportService;
import java.util.List;

/**
 *
 * @author PC Sakda
 */
public class TestReportSale {
    public static void main(String[] args) {
        ReportService reportService = new ReportService();
//        List<ReportSale> report = reportService.getReportSaleByDay();
//        for(ReportSale r:report){
//            System.out.println(r);
//        }
        
        List<ReportSale> reportMonth = reportService.getReportSaleByMonth(2013);
        for(ReportSale r:reportMonth){
            System.out.println(r);
        }
    }
}
